
import static java.util.Arrays.stream;
import java.util.List;
import static java.util.stream.Collectors.toList;
import vtk.vtkNativeLibrary;
import static vtk.vtkNativeLibrary.values;

public class TestVTKInstall {

    static boolean isEveryThingLoaded = true;

    // -----------------------------------------------------------------
    // Load VTK library and print which library was not properly loaded
    public static void main(String s[]) {
        boolean loaded = true;
        //if (!LoadAllVTKNativeLibraries())
        if (!LoadAllVTKNativeLibraries()) {
            List<String> loadedNatives
                    = stream(values())
                    .filter(vtkNativeLibrary::IsLoaded)
                    .map(vtkNativeLibrary::GetLibraryName)
                    .collect(toList());
            loaded = !loadedNatives.isEmpty();
            loadedNatives.stream().forEach(System.out::println);
        }
        String status = loaded ? "Successfully" : "Failed";
        System.out.println(status + " installed VTK!");
    }

    //This is the same as vtkNativeLibrary::LoadAllNativeLibraries except it
    //doesn't throw an exception on failed load
    public static boolean LoadAllVTKNativeLibraries() {

        stream(values())
                .filter(vtkNativeLibrary::IsBuilt)
                .forEach(nativeLibrary -> {
                    try {
                        nativeLibrary.LoadLibrary();
                    } catch (UnsatisfiedLinkError e) {
                        isEveryThingLoaded = false;
                        //don't print stack trace here!!!
                    }
                });

        return isEveryThingLoaded;
    }
}
